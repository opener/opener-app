OPENER - Mobile Android Client App
==================================

The OPENER App is an app to collect and map accessibility features at German bus, tram and train stops.
This is realised by selecting a stop at a map and answering related questions.
Furthermore, geometric features, such as the bus stop height or the pavement width, can be measured by
an Augmented Reality Measure Feature on [ARCore supported devices](https://developers.google.com/ar/discover/supported-devices).

Data collected by this app will be published under an Open Data license (e.g. Creative Commons) and can be freely used.
The decision about the specific data license is not made yet, but compatibility to the OpenStreetMap Data license
ODbL is required.

Further information can be found at the German-speaking [project website of OPENER](https://www.bmvi.de/SharedDocs/DE/Artikel/DG/mfund-projekte/opener.html).

The development of this app is sponsored by the Federal Ministry of Transport and Digital Infrastructure under the project ID VB18F1016A 
within the digital innovation program mFUND.

# Screenshots

<img src="docs/imgs/mapFragmentHOT.png" width="170"/> 
<img src="docs/imgs/questions.png" width="170"/>
<img src="docs/imgs/measureQuestion.png" width="170"/>
<img src="docs/imgs/infoFragment.png" width="170"/>
<img src="docs/imgs/infoImage.png" width="170"/>

 
# Current Development State and Notes

- Currently, the app is only working offline and does not persistently store the answers
- Only a few example stops at the Chemnitz University of Technology campus are included
- The primarily supported language is German, English is only sparsely maintained

# Next Steps

- Implementation of the server-side database and web app
- Import of all stops available for Saxony

# Download 

Currently available as closed beta on Google Play:

<dl>
<a href='https://play.google.com/store/apps/details?id=org.mytuc.etit.sse.openerapp&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
<img width="350" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
</dl>

Google Play and the Google Play logo are trademarks of Google LLC.
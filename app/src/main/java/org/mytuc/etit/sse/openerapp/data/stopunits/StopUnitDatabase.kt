package org.mytuc.etit.sse.openerapp.data.stopunits

/**
 * Created on 2019-09-25 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
object StopUnitDatabase {

    val stopUnits = arrayOf(
            StopUnitData("DHID de:14511:30222:1:CN", "Rosenbergstraße", "ChemnitzBahn Nord",
                    50.818023, 12.929236),
            StopUnitData("DHID de:14511:30222:1:CS", "Rosenbergstraße", "ChemnitzBahn Süd",
                    50.817506, 12.92938),
            StopUnitData("DHID de:14511:30222:2:BN", "Rosenbergstraße", "Bus Nord",
                    50.81804, 12.929338),
            StopUnitData("DHID de:14511:30222:2:BS", "Rosenbergstraße", "Bus Süd",
                    50.817569, 12.929209),
            StopUnitData("DHID de:14511:30222:1:TN", "Rosenbergstraße", "Tram Nord",
                    50.817751, 12.929416),
            StopUnitData("DHID de:14511:30222:1:TS", "Rosenbergstraße", "Tram Süd",
                    50.817824, 12.92929),
            StopUnitData("DHID de:14511:30220:1:2A", "TU Campus", "ChemnitzBahn Süd 2A",
                    50.813465, 12.931051),
            StopUnitData("DHID de:14511:30220:1:2B", "TU Campus", "Tram Süd 2B",
                    50.813709, 12.930808),
            StopUnitData("DHID de:14511:30220:1:3A", "TU Campus", "ChemnitzBahn Nord 3A",
                    50.813925, 12.930719),
            StopUnitData("DHID de:14511:30220:1:3B", "TU Campus", "Tram Nord 3B",
                    50.813658, 12.930979),
            StopUnitData("DHID de:14511:30220:2:1", "TU Campus", "Bus Süd 1",
                    50.813721, 12.930683),
            StopUnitData("DHID de:14511:30220:2:4", "TU Campus", "Bus Nord 4",
                    50.813936, 12.930844))
}
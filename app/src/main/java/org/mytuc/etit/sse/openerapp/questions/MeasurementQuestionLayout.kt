package org.mytuc.etit.sse.openerapp.questions

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.navigation.Navigation
import org.mytuc.etit.sse.openerapp.data.questions.MeasurementQuestion
import org.mytuc.etit.sse.openerapp.measure.MeasureUnit
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel
import com.google.ar.core.ArCoreApk
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.component_question_measurement.view.*
import org.mytuc.etit.sse.openerapp.R

/**
 * Created on 2019-06-18 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class MeasurementQuestionLayout(context: Context, attributeSet: AttributeSet?) : ConstraintLayout(context, attributeSet) {

    lateinit var model: OpenerViewModel
    lateinit var measurementQuestion: MeasurementQuestion

    constructor(context: Context) : this(context, null)

    init {
        inflate(context, R.layout.component_question_measurement, this)
    }

    fun initializeLayout() {
        val radioButtonMeasurement = findViewById<RadioButton>(R.id.radioButtonMeasurement)
        val radioButtonDonotKnow = findViewById<RadioButton>(R.id.radioButton_donot_know)
        val editMeasurement = findViewById<EditText>(R.id.editMeasurement)

        val radioGroup = findViewById<RadioGroup>(R.id.measurementRadioGroup)

        radioButtonMeasurement.setOnClickListener{
            editMeasurement.requestFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editMeasurement, 0)
        }

        editMeasurement.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus) {
                radioButtonMeasurement.isChecked = true
            } else {
                val measuredValue = editMeasurement.text.toString()
                if(measuredValue.isEmpty()) { // if there is no entry go back to do not know button
                    radioButtonDonotKnow.isChecked = true
                } else {
                    val answer = measurementQuestion.answer(measuredValue)
                    model.answers[answer.first] = answer.second
                }


                hideKeyboard(v)
                editMeasurement.clearFocus()
            }
        }

        editMeasurement.setOnEditorActionListener {v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                val measuredValue = v.text.toString()
                if(measuredValue.isEmpty()) { // if there is no entry go back to do not know button
                    radioButtonDonotKnow.isChecked = true
                } else {
                    val answer = measurementQuestion.answer(measuredValue)
                    model.answers[answer.first] = answer.second
                }
                hideKeyboard(v)
                editMeasurement.clearFocus()
                true

            } else {
                false
            }
        }

        val dontKnowButton = findViewById<RadioButton>(R.id.radioButton_donot_know)
        dontKnowButton.setOnClickListener{ button ->

            // check for value in edit text and ask the user for removal of this value
            if(editMeasurement.text.isNotEmpty()) {
                showRemovalDialog()

            }

            hideKeyboard(button)
            editMeasurement.clearFocus()
            radioGroup.requestFocus()
        }

        val titleUnit = findViewById<TextView>(R.id.text_unit)
        titleUnit.setText(measurementQuestion.measureUnit.stringId)

        val measureButton = findViewById<ImageButton>(R.id.buttonMeasurement)
        measureButton.setOnClickListener {
            val action = DelfiQuestionFragmentDirections
                    .actionDelfiQuestionFragmentToMeasureFragment(measurementQuestion.measureMode, measurementQuestion.delfiId)
            val navController = Navigation.findNavController(it)
            navController.navigate(action)
        }


        maybeEnableArButton()
    }

    fun restoreAnswer() {
        val answer = model.answers[measurementQuestion.delfiId]
        answer?.also {
            val editMeasurement = findViewById<EditText>(R.id.editMeasurement)
            editMeasurement.setText(answer)
            val radioButtonMeasurement = findViewById<RadioButton>(R.id.radioButtonMeasurement)
            radioButtonMeasurement.isChecked = true
        }
    }

    /**
     * Helper method to hide the keyboard
     * Source: https://stackoverflow.com/a/19828165
     *
     */
    private fun hideKeyboard(view: View) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun maybeEnableArButton() {
        val measureButton = findViewById<ImageButton>(R.id.buttonMeasurement)
        val availability = ArCoreApk.getInstance().checkAvailability(context)
        if (availability.isTransient) {
            // Re-query at 5Hz while compatibility is checked in the background.
            Handler().postDelayed({ maybeEnableArButton() }, 200)
        }
        if (availability.isSupported && measurementQuestion.measureUnit == MeasureUnit.CM) {
            measureButton.visibility = View.VISIBLE
            measureButton.isEnabled = true
            // indicator on the button.
        } else { // Unsupported or unknown.
            measureButton.visibility = View.INVISIBLE
            measureButton.isEnabled = false
        }
    }

    private fun showRemovalDialog() {
        val builder: AlertDialog.Builder? = context?.let {
            AlertDialog.Builder(it)
        }
        builder?.apply {
            setMessage(R.string.dialog_remove_measurement)
            setPositiveButton(R.string.ok) {dialog, id ->
                editMeasurement.text.clear()
                model.answers.remove(measurementQuestion.delfiId)
                dialog.dismiss()
            }
            setNegativeButton(R.string.cancel) { dialog, id ->
                dialog.dismiss()
                radioButtonMeasurement.isChecked = true
            }
            create().show()
        }
    }


}
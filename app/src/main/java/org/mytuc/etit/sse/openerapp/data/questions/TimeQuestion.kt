package org.mytuc.etit.sse.openerapp.data.questions

class TimeQuestion(delfiID: Int,
                   questionTextId: Int,
                   infoDescriptionId: Int,
                   answerExamples : List<Pair<Int, String>>
) : Question(delfiID, questionTextId, infoDescriptionId, answerExamples) {
}
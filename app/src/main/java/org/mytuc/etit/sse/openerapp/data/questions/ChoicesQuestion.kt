package org.mytuc.etit.sse.openerapp.data.questions

/**
 * A Question that can be answered by selecting a given choice.
 * [questionTextId] is the question itself
 * [infoDescriptionId] provides additional information about the possible answers.
 * [choicesExamples] is a list of pairs with a ids to string resources on the left side and a path
 *                   to a directory that contains example image for each choice.
 *
 */
class ChoicesQuestion(delfiID: Int,
                      questionTextId: Int,
                      infoDescriptionId: Int,
                      choicesExamples : List<Pair<Int, String>>
) : Question(delfiID ,questionTextId, infoDescriptionId, choicesExamples) {
}
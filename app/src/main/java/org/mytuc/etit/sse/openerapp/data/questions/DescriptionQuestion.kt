package org.mytuc.etit.sse.openerapp.data.questions

class DescriptionQuestion(delfiID: Int,
                          questionTextId: Int,
                          infoDescription: Int,
                          answerExamples : List<Pair<Int, String>>,
                          //hint how to answer current question
                          val descriptionHint: Int
) : Question(delfiID, questionTextId, infoDescription, answerExamples)
package org.mytuc.etit.sse.openerapp.questions

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import org.mytuc.etit.sse.openerapp.R
import org.mytuc.etit.sse.openerapp.data.questions.DescriptionQuestion
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel

/**
 * Created on 2019-10-11 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class DescriptionQuestionLayout(context: Context, attributeSet: AttributeSet?) : LinearLayout(context, attributeSet) {

    lateinit var descriptionQuestion: DescriptionQuestion
    lateinit var model: OpenerViewModel


    constructor(context: Context) : this(context, null)

    init {
        inflate(context, R.layout.component_question_description, this)
    }

    fun initializeLayout() {
        val editTextDescription = findViewById<EditText>(R.id.editTextDescription)
        editTextDescription.setHint(descriptionQuestion.descriptionHint)

        editTextDescription.setOnEditorActionListener{v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE ) {
                val descriptionValue = v.text.toString()
                val answer = descriptionQuestion.answer(descriptionValue)
                model.answers[answer.first] = answer.second
                hideKeyboard(v)
                editTextDescription.clearFocus()
                true

            } else {
                false
            }
        }

        editTextDescription.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                val descriptionValue = editTextDescription.text.toString()
                val answer = descriptionQuestion.answer(descriptionValue)
                model.answers[answer.first] = answer.second
                hideKeyboard(v)
                editTextDescription.clearFocus()
            }
        }
    }

    fun restoreAnswer() {
        val answer = model.answers[descriptionQuestion.delfiId]
        answer?.also {
            val editTextDescription = findViewById<EditText>(R.id.editTextDescription)
            editTextDescription.setText(answer)
        }
    }

    /**
     * Helper method to hide the keyboard when clicking on any area
     * Source: https://stackoverflow.com/a/19828165
     *
     */
    private fun hideKeyboard(view: View) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
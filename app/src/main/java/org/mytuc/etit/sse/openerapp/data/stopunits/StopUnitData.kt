package org.mytuc.etit.sse.openerapp.data.stopunits

const val NO_DHID = "NO_ID"

/**
 * Created on 2019-05-21 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class StopUnitData(val dhid: String, val title: String,
                   val description: String,
                   val latitude: Double, val longitude: Double)
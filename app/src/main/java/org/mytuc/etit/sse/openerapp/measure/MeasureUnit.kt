package org.mytuc.etit.sse.openerapp.measure

import org.mytuc.etit.sse.openerapp.R

/**
 * Created on 2019-08-02 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
enum class MeasureUnit(val stringId: Int) {

    CM(R.string.unit_cm), // centimeters
    KG(R.string.unit_kg),  // kilograms
    M2(R.string.unit_m2), // square meters
    NUM(R.string.unit_number),  // countable, number
    PERCENT(R.string.unit_percent),  // percent
    SEC(R.string.unit_s)  // seconds

}

package org.mytuc.etit.sse.openerapp.info

import android.net.Uri

/**
 * Created on 2019-08-05 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
interface ImageUrlResolver {

    fun resolveUris(imagesSrcDirPath: String) : List<Uri>
}
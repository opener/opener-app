package org.mytuc.etit.sse.openerapp.model

import androidx.lifecycle.ViewModel
import org.mytuc.etit.sse.openerapp.data.stopunits.NO_DHID

val NO_AR_MEASUREMENT = ArMeasuredAttribute(0,0f)

/**
 * [OpenerViewModel] is a model class that stores dynamic data of the current app session.
 * This class is used to share application data (such as the currently selected stop unit id)
 * between the fragments.
 *
 * Information about the usage and the background of this class can be found under:
 * https://developer.android.com/topic/libraries/architecture/viewmodel#kotlin
 *
 * Created on 2019-09-25 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class OpenerViewModel : ViewModel() {

    var stopUnitId : String = "NO_ID" // Todo: Is there any reset during a session???

    var arMeasuredAttribute = NO_AR_MEASUREMENT

    // todo: construct a datastructure for all delfi questions? ids?
    // todo: how long should this data be stored??? when has this data to be pushed?
    // this class should only store data which should be visualised within the fragments
    // it is no general, persistent storage for data of this app! this has to be written to disk or
    // to a server

    // map of DELFI question id, to answer string
    val answers = mutableMapOf<Int, String>()

    /**
     * Call this to clean up all question answers and also the stop unit id.
     */
    fun cleanUpAnswers() {
        stopUnitId = NO_DHID
        arMeasuredAttribute = NO_AR_MEASUREMENT
        answers.clear()
    }
}
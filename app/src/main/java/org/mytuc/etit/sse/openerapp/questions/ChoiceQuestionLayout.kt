package org.mytuc.etit.sse.openerapp.questions

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import org.mytuc.etit.sse.openerapp.R
import org.mytuc.etit.sse.openerapp.data.questions.ChoicesQuestion
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel

/**
 * Created on 2019-10-09 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class ChoiceQuestionLayout(context: Context, attributeSet: AttributeSet?) : LinearLayout(context, attributeSet) {

    private val radioButtons = mutableListOf<RadioButton>()
    lateinit var choicesQuestion: ChoicesQuestion
    lateinit var model: OpenerViewModel


    constructor(context: Context) : this(context, null)

    init {
        inflate(context, R.layout.component_question_choice, this)
        orientation = VERTICAL
    }

    fun initializeLayout() {
        // workaround: Because Androids [android.widget.RadioGroup] does not support nested layouts (multiple
        // rows such as is used below), the deselection of RadioButtons does not work. This is now handled by
        // a reference of the used buttons and an OnClickListener that deselects the not pressed buttons.
        val radioButtonDontKnow = this.findViewById<RadioButton>(R.id.radioButton_donot_know_choice)

        // ATTENTION: Explicitly initialize radiobutton with checked is true to prevent firing an
        // oncheckedchange event with true as parameter. This would lead to a removal of the
        // previously stored answer.
        radioButtonDontKnow.isChecked = true
        radioButtonDontKnow.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                //remove answer from model
                model.answers.remove(choicesQuestion.delfiId)
                deselectOtherButtons(buttonView)
            }
        }
        radioButtons.add(radioButtonDontKnow)

        val choiceResIds = choicesQuestion.answerExamples.map {pairIdPicPaths -> pairIdPicPaths.first }
        // iterate over choices of this question and create a choice button for this
        // only 3 choices per row are generated
        for (index in choiceResIds.indices) {
            val indexChoice = index + 1 // choice "do not know" also counts!
            val rowIndex = (indexChoice) / 3 // integer division to get the required choice row index
            var choicesLine = getChildAt(rowIndex) as LinearLayout?
            if (choicesLine == null) {
                choicesLine = LayoutInflater.from(context).inflate(R.layout.template_linear_layout_horizontal,
                        this, false) as LinearLayout
                addView(choicesLine)
            }

            //Initialize each radiobutton
            val radioButton = LayoutInflater.from(context).inflate(R.layout.template_radio_button,
                    choicesLine, false) as RadioButton
            choicesLine.addView(radioButton)
            radioButton.id = choiceResIds[index]
            radioButtons.add(radioButton)

            val choiceTextResId = choiceResIds[index]
            radioButton.setText(choiceTextResId)

            /*
            radioButton.setOnFocusChangeListener { view, hasFocus ->
                requestFocusAndCheckButton(view, hasFocus, refRadioButtons)
            }
             */
            radioButton.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    deselectOtherButtons(buttonView)
                    val answer = choicesQuestion.answer(radioButton.id.toString())
                    model.answers[answer.first] = answer.second
                }
            }

        }
    }

    fun restoreAnswer() {
        val activeChoiceId = model.answers[choicesQuestion.delfiId]?.toInt()
        radioButtons.forEach { radioButton ->
            if (radioButton.id == activeChoiceId) {
                radioButton.isChecked = true
            } else if (radioButton.id == R.id.radioButton_donot_know_choice && activeChoiceId == null) {
                radioButton.isChecked = true
            }

        }
    }

    /**
     * Method to deselect [otherButtons] than the currently [pressedButton].
     * This used for layouts where the Androids RadioGroup does not work (nested layouts within RadioGroups).
     */
    private fun deselectOtherButtons(pressedButton: View) {
        val iteratorButtons = radioButtons.iterator()
        while(iteratorButtons.hasNext()) {
            val radioButton = iteratorButtons.next()

            if(radioButton === pressedButton) {
                // skip currently pressed button
                continue
            }
            radioButton.isChecked = false
        }
    }

}
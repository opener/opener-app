package org.mytuc.etit.sse.openerapp.info

import android.net.Uri
import android.util.Log
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.File
import java.lang.Exception


/**
 * Created on 2019-08-05 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class HttpUrlResolver(private val baseURL: String) : ImageUrlResolver {

    override fun resolveUris(imagesSrcDirPath: String): List<Uri> {
        val imageUris = arrayListOf<Uri>()


        var doc : Document? = null
        try {
            doc = Jsoup.connect(baseURL + imagesSrcDirPath).get()
        } catch (exception: Exception) {
            Log.e(HttpUrlResolver::class.toString(), exception.localizedMessage)
        }
        if (doc == null) {
            return imageUris
        }

        val table = doc.select("table").first()
        // find all href attributes that have an image file ending (jsoup CSS Selector with regular expression)
        // this finds two href attributes for each image name
        val refsA = table.select("a[href~=(?i)\\.(png|jpe?g)]")
        val imageNames = arrayListOf<String>()

        val basePath = "/" + baseURL.substringAfter("https://") + imagesSrcDirPath

        refsA.forEach {

            val imageName = it.attr("href")
            // only extract image name once
            if (!imageNames.contains(imageName)) {
                imageNames.add(imageName)
                val uri =  Uri.parse(baseURL + imagesSrcDirPath + File.separator +  imageName)
                imageUris.add(uri)
            }
        }
        return imageUris
    }


}
package org.mytuc.etit.sse.openerapp.maps

import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase
import org.osmdroid.tileprovider.tilesource.XYTileSource

/**
 * Created on 2019-10-14 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */

val HOT: OnlineTileSourceBase = XYTileSource("HOT", 10, 20, 256, ".png",
            arrayOf("https://a.tile.openstreetmap.fr/hot/", "https://b.tile.openstreetmap.fr/hot/", "https://c.tile.openstreetmap.fr/hot/"), "© OpenStreetMap contributors")

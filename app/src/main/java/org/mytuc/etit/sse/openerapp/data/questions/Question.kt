package org.mytuc.etit.sse.openerapp.data.questions

import org.mytuc.etit.sse.openerapp.R

abstract class Question(
        val delfiId: Int,
        val questionTextId: Int,
        var infoDescriptionId: Int,
        // answerExamples contains specific answer with paths to example images
        var answerExamples : List<Pair<Int, String>>) {

    val donotKnow = R.string.ans_dontknow

    fun answer(property: String) : Pair<Int, String> {
        return Pair(delfiId, property)
    }

}
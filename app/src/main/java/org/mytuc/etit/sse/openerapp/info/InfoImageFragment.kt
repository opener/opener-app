package org.mytuc.etit.sse.openerapp.info


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import org.mytuc.etit.sse.openerapp.R


/**
 * A [Fragment] subclass to show full screen images.
 *
 */
class InfoImageFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val argumentsSafe = arguments

        argumentsSafe?: return //
        val fragmentArgs = InfoImageFragmentArgs.fromBundle(argumentsSafe)

        val infoImage = view.findViewById(R.id.info_image) as SimpleDraweeView
        infoImage.setImageURI(fragmentArgs.imageUri)
    }


}

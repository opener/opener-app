package org.mytuc.etit.sse.openerapp.info

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.mytuc.etit.sse.openerapp.R


class InfoFragment : Fragment() {

    val imageUrlResolver : ImageUrlResolver = HttpUrlResolver("https://www-user.tu-chemnitz.de/~grait/opener/")  //AssetsUrlResolver(context)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val argumentsCopy = arguments
        if(argumentsCopy != null) {
            val actionArgs = InfoFragmentArgs.fromBundle(argumentsCopy)

            val infoQuestion = view.findViewById(R.id.info_question_yes_no_title) as TextView
            infoQuestion.setText(actionArgs.questionID)

            val infoQuestionDesc = view.findViewById(R.id.info_question_yes_no_desc) as TextView
            infoQuestionDesc.setText(actionArgs.questionDescriptionID)

            val exampleContainerList = view.findViewById(R.id.exampleContainerList) as LinearLayout

            if(actionArgs.choiceIds.size != actionArgs.imagePaths.size) return

            for (i in actionArgs.choiceIds.indices) {

                val exampleContainer = layoutInflater.inflate(
                        R.layout.fragment_info_example_answer_container, exampleContainerList, false) as LinearLayout

                val exampleTitle = exampleContainer.findViewById(R.id.exampleAnswerTitle) as TextView
                exampleTitle.setText(actionArgs.choiceIds[i])
                val assets = context?.assets
                if (assets != null) {
                    val imageGallery = exampleContainer.findViewById(R.id.gallery_container) as LinearLayout
                    GlobalScope.launch {
                        val imageUris = imageUrlResolver.resolveUris(actionArgs.imagePaths[i])
                        configureImageSources(imageUris, imageGallery)
                    }
                }

                exampleContainerList.addView(exampleContainer)
            }
        }
    }

    private fun configureImageSources(imageUrls: List<Uri>, imagesDstGallery: LinearLayout) {
        val uiHandler = Handler(Looper.getMainLooper())
        imageUrls.forEach {
            val uri =  it
            val imageView = LayoutInflater.from(context)
                    .inflate(R.layout.simple_drawee_container, imagesDstGallery, false) as SimpleDraweeView
            imageView.setOnClickListener {
                val directionAction = InfoFragmentDirections.actionInfoFragmentYesNoToInfoImageFragment(uri.toString())
                Navigation.findNavController(it).navigate(directionAction)
            }

            val request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setResizeOptions(ResizeOptions(150, 150))
                    .build()
            imageView.controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(imageView.controller)
                    .setImageRequest(request)
                    .build()
            uiHandler.post{imagesDstGallery.addView(imageView)}
        }
    }
}


package org.mytuc.etit.sse.openerapp.data.questions

import org.mytuc.etit.sse.openerapp.measure.MeasureMode
import org.mytuc.etit.sse.openerapp.measure.MeasureUnit

class MeasurementQuestion(delfiID: Int,
                          questionTextId: Int,
                          infoDescriptionId: Int,
                          answerExamples : List<Pair<Int, String>>,
                         // unit of the measurement, e.g. kg, cm, ...
                          val measureUnit: MeasureUnit,
                          val measureMode: MeasureMode
) : Question(delfiID, questionTextId, infoDescriptionId, answerExamples)
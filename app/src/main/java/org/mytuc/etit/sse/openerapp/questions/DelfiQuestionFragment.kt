package org.mytuc.etit.sse.openerapp.questions

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import org.mytuc.etit.sse.openerapp.data.questions.*
import android.widget.ScrollView
import androidx.core.view.children
import androidx.lifecycle.ViewModelProvider
import org.mytuc.etit.sse.openerapp.R
import org.mytuc.etit.sse.openerapp.data.stopunits.StopUnitDatabase
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel


/**
 * Created on 2019-05-15 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class DelfiQuestionFragment : Fragment() {

    private lateinit var model: OpenerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        // load fragment data
        model = activity?.run {
            ViewModelProvider(this).get(OpenerViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_questions, container, false)
        val argumentsCopy = arguments
        if(argumentsCopy != null) {
            val stopUnitData = StopUnitDatabase.stopUnits.firstOrNull { stopUnitData -> stopUnitData.dhid == model.stopUnitId }
            stopUnitData?.also {
                val textViewTitle = view.findViewById(R.id.stop_unit_title) as TextView
                textViewTitle.text = stopUnitData.title

                val textViewDhid = view.findViewById(R.id.stop_unit_description) as TextView
                textViewDhid.text = stopUnitData.description
            }
        }
        createQuestionsLayout(view, QuestionDatabase.questions)
        return view
    }


    override fun onResume() {
        super.onResume()
        // todo restore answers
        val layoutQuestions = view?.findViewById(R.id.questionListLayout) as LinearLayout
        layoutQuestions.children.forEach { view ->
            val choiceQuestionLayout = view.findViewById<ChoiceQuestionLayout>(R.id.choiceGroup)
            choiceQuestionLayout?.restoreAnswer()

            val measurementQuestionLayout = view.findViewById<MeasurementQuestionLayout>(R.id.measurementGroup)
            measurementQuestionLayout?.restoreAnswer()

            val descriptionQuestionLayout = view.findViewById<DescriptionQuestionLayout>(R.id.descriptionQuestionContent)
            descriptionQuestionLayout?.restoreAnswer()

        }
    }

    private fun loadAnswers() {
        if (model.stopUnitId == "NO_ID") return

        val preferenceFileName = getString(R.string.delfi_answers_file_prefix) + model.stopUnitId

        val sharedPref = activity?.getSharedPreferences(
                preferenceFileName , Context.MODE_PRIVATE)

    }

    /**
     * Dynamically create the UI of the DELFI questions based on a given question database
     *
     */
    private fun createQuestionsLayout(parentView: View, questions: List<Question>) : LinearLayout {

        // WORKAROUND: https://stackoverflow.com/a/8101614
        val view = parentView.findViewById(R.id.scrollViewQuestions) as ScrollView
        view.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
        view.isFocusable = true
        view.isFocusableInTouchMode = true
        view.setOnTouchListener { v, event ->
            v.requestFocusFromTouch()
            false
        }

        val layoutQuestions = parentView.findViewById(R.id.questionListLayout) as LinearLayout

        var questionLayout: View
        for(index in questions.indices) {
            val questionData = questions[index]

            val coloredBackground = index%2 == 1

            if(questionData is ChoicesQuestion) {
                questionLayout = createQuestionLayout(questionData, layoutQuestions, coloredBackground)
                layoutQuestions.addView(questionLayout)
            }
            if(questionData is MeasurementQuestion) {
                questionLayout = createQuestionLayout(questionData, layoutQuestions, coloredBackground)
                layoutQuestions.addView(questionLayout)
            }

            if (questionData is DescriptionQuestion) {
                questionLayout = createQuestionLayout(questionData, layoutQuestions, coloredBackground)
                layoutQuestions.addView(questionLayout)
            }

        }
        return layoutQuestions
    }

    /**
     * Initialize choice question layout
     *
     */
    private fun createQuestionLayout(choicesQuestion: ChoicesQuestion, parentView: ViewGroup,
                                     coloredBackground: Boolean) : View {
        val questionLayout =
                layoutInflater.inflate(R.layout.question_choice, parentView, false)

        fillCommonQuestionLayout(questionLayout, choicesQuestion, coloredBackground)
        val choiceQuestionLayout = questionLayout.findViewById<ChoiceQuestionLayout>(R.id.choiceGroup)
        choiceQuestionLayout.choicesQuestion = choicesQuestion
        choiceQuestionLayout.model = model
        choiceQuestionLayout.initializeLayout()

        return questionLayout
    }

    /**
     * Initialize measurement question layout
     *
     */
    private fun createQuestionLayout(measurementQuestion: MeasurementQuestion, parentView: ViewGroup,
                                     coloredBackground: Boolean) : View {
        val questionLayout =
                layoutInflater.inflate(R.layout.question_measurement, parentView, false)

        fillCommonQuestionLayout(questionLayout, measurementQuestion, coloredBackground)
        val measurementQuestionLayout = questionLayout.findViewById<MeasurementQuestionLayout>(R.id.measurementGroup)
        measurementQuestionLayout.model = model
        measurementQuestionLayout.measurementQuestion = measurementQuestion
        measurementQuestionLayout.initializeLayout()
        return questionLayout
    }

    /**
     * Initialize description question layout for [DescriptionQuestion]s
     *
     */
    private fun createQuestionLayout(descriptionQuestion: DescriptionQuestion, parentView: ViewGroup,
                                     coloredBackground: Boolean) : View {
        val questionLayout =
                layoutInflater.inflate(R.layout.question_description, parentView, false)

        fillCommonQuestionLayout(questionLayout, descriptionQuestion, coloredBackground)
        val descriptionQuestionLayout = questionLayout.findViewById<DescriptionQuestionLayout>(R.id.descriptionQuestionContent)
        descriptionQuestionLayout.descriptionQuestion = descriptionQuestion
        descriptionQuestionLayout.model = model
        descriptionQuestionLayout.initializeLayout()
        return questionLayout
    }


    /**
     * Initialize a common question view attributes
     *
     */
    private fun fillCommonQuestionLayout(questionLayout: View, question: Question, colorBackground: Boolean) {
        questionLayout.id = question.delfiId

        val title = questionLayout.findViewById(R.id.questionTitle) as TextView
        title.setText(question.questionTextId)

        if(colorBackground) {
            questionLayout.setBackgroundColor(resources.getColor(R.color.colorQuestionBackground))
        }

        val infoButton = questionLayout.findViewById(R.id.infoButton) as ImageButton
        infoButton.setOnClickListener {

            val action = DelfiQuestionFragmentDirections.actionDelfiQuestionFragmentToInfoFragmentYesNo(
                    question.questionTextId,
                    question.infoDescriptionId,
                    question.answerExamples.map { it.second }.toTypedArray(),
                    question.answerExamples.map { it.first }.toIntArray())

            val navController = Navigation.findNavController(it)
            navController.navigate(action)
        }

    }

    /**
     * Request focus to the pressed radio button to clear the focus of an EditText, as it is used within the
     * measurement question. This is used to hide the keyboard if a choice question is clicked.
     */
    private fun requestFocusAndCheckButton(pressedButton: View, hasFocus: Boolean, otherButtons: List<RadioButton>) {
        if(hasFocus) {
            pressedButton.clearFocus()
            (pressedButton as RadioButton).isChecked = true
            deselectOtherButtons(pressedButton, otherButtons)
        }
    }



    /**
     * Method to deselect [otherButtons] than the currently [pressedButton].
     * This used for layouts where the Androids RadioGroup does not work (nested layouts within RadioGroups).
     */
    private fun deselectOtherButtons(pressedButton: View, otherButtons: List<RadioButton>) {
        val iteratorButtons = otherButtons.iterator()
        while(iteratorButtons.hasNext()) {
            val radioButton = iteratorButtons.next()

            if(radioButton === pressedButton) {
                // skip currently pressed button
                continue
            }
            radioButton.isChecked = false
        }
    }
}
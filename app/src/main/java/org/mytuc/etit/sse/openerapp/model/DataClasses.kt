package org.mytuc.etit.sse.openerapp.model

/**
 * Created on 2019-09-26 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */

class ArMeasuredAttribute(val attributeId: Int, val attributeValue: Float)
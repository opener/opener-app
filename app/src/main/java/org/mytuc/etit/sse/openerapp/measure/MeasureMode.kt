package org.mytuc.etit.sse.openerapp.measure


/**
 * Created on 2019-07-31 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */

enum class MeasureMode {
    DISTANCE, ELEVATION, NOTHING
}


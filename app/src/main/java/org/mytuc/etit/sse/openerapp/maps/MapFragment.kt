package org.mytuc.etit.sse.openerapp.maps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import org.mytuc.etit.sse.openerapp.R
import org.mytuc.etit.sse.openerapp.data.stopunits.StopUnitData
import org.mytuc.etit.sse.openerapp.data.stopunits.StopUnitDatabase
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow

/**
 * Created on 2019-05-18 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class MapFragment : Fragment() {

    private lateinit var mapView : MapView
    private lateinit var model: OpenerViewModel

    private var activeStopPopup: StopInfoWindow? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        model = activity?.run {
            ViewModelProvider(this).get(OpenerViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        // set user agent identification for osmdroid library to opener app package name
        Configuration.getInstance().userAgentValue = context?.packageName

        // deactivate back button in this fragment to prevent the app from closing by pressing back
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            showQuitDialog()
        }
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mapView = inflater.inflate(R.layout.fragment_map, container, false) as MapView
        mapView.apply {
            setTileSource(HOT)
            setMultiTouchControls(true)

            // Add an single tap event handler that closes an open pop up window when the map is clicked
            overlays.add(MapEventsOverlay(object: MapEventsReceiver {
                override fun longPressHelper(p: GeoPoint?): Boolean {
                    return false
                }

                override fun singleTapConfirmedHelper(p: GeoPoint?): Boolean {
                    activeStopPopup?.let{
                        it.close()
                    }
                    return false
                }
            }
            ))
        }
        mapView.controller.apply {
            setCenter(GeoPoint(50.8150,12.9334)) //tu campus
            setZoom(16.0)
        }
        StopUnitDatabase.stopUnits.forEach { createStopIcons(it) }
        return mapView
    }

    override fun onStart() {
        super.onStart()
        // clean up delfi question at each entry in this map fragment to have a clean environment
        model.cleanUpAnswers()
    }

    /**
     * Create the stop icons as map overlay.
     * Documentation for osmdroid marker api can be found under:
     * https://github.com/osmdroid/osmdroid/wiki/Markers,-Lines-and-Polygons
     */
    private fun createStopIcons(stopUnitData: StopUnitData) {
        //your items
        val marker = Marker(mapView)
        marker.id = stopUnitData.dhid
        marker.title = stopUnitData.title
        marker.snippet = stopUnitData.description
        marker.position = GeoPoint(stopUnitData.latitude, stopUnitData.longitude)
        marker.setOnMarkerClickListener { marker, mapView ->
            if(activeStopPopup != null) {
                activeStopPopup?.close()
            }
            activeStopPopup = StopInfoWindow(R.layout.info_window_stop, mapView)
            marker.infoWindow = activeStopPopup
            marker.showInfoWindow()
            mapView.controller.animateTo(marker.position)
            return@setOnMarkerClickListener true
        }
        marker.icon = resources.getDrawable(R.drawable.ic_bus_stop, context?.theme)
        marker.infoWindow = StopInfoWindow(R.layout.info_window_stop, mapView)
        mapView.overlays.add(marker)

    }

    /**
     * Override and modify layout from:
     * https://github.com/osmdroid/osmdroid/blob/master/osmdroid-android/src/main/res/layout/bonuspack_bubble.xml
     */
    private inner class StopInfoWindow(resId: Int, mapView: MapView) : MarkerInfoWindow(resId, mapView) {

        override fun onOpen(item: Any?) {
            super.onOpen(item)


            val button = mView.findViewById<Button>(R.id.bubble_moreinfo)
            button.setText(R.string.button_stop_unit_map_text)

            button.setOnClickListener {
                val navController = Navigation.findNavController(it)

                val markerReference = this@StopInfoWindow.markerReference
                model.stopUnitId = markerReference.id
                val action = MapFragmentDirections.actionMapFragmentToDelfiQuestionFragment(
                        markerReference.id, markerReference.title, markerReference.snippet)
                navController.navigate(action)
            }
            button.visibility = View.VISIBLE

        }
    }

    /**
     * Show quit confirm dialog for closing the app.
     */
    private fun showQuitDialog() {
        val builder: AlertDialog.Builder? = context?.let {
            AlertDialog.Builder(it)
        }
        builder?.apply {
            setMessage(R.string.dialog_quit_app)
            setPositiveButton(R.string.ans_yes) {dialog, id ->
                activity?.finishAffinity()
                }
            setNegativeButton(R.string.ans_no) { dialog, id ->
                dialog.dismiss()
            }
            create().show()
        }
    }
}

package org.mytuc.etit.sse.openerapp.intro


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_intro.*
import org.mytuc.etit.sse.openerapp.R


/**
 * A simple [Fragment] subclass.
 *
 */
class IntroFragment : Fragment() {

    private val permissionsRequired = hashMapOf(10 to Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        buttonRequestPermissions.setOnClickListener{
            requestPermissions()
        }

    }

    override fun onResume() {
        //check if all permissions are given
        if(allPermissionGranted()) {
            listLayoutPermissions.visibility = View.GONE
            buttonRequestPermissions.visibility = View.GONE
            Handler().postDelayed({
                navigateToMapFragment()
            }, 2000)

        } else {
            // check for permissions
            requestPermissions()
        }
        super.onResume()
    }

    private fun navigateToMapFragment() {
        val navController = Navigation.findNavController(view!!)
        val action = IntroFragmentDirections.actionIntroFragmentToMapFragment()
        navController.navigate(action)
    }


    /**
     * Iterate over all required permissions and check if they are granted.
     * Return true if all are granted else false.
     *
     */
    private fun allPermissionGranted() : Boolean {
        var allPermissionsAllowed = true
        val activity = activity
        if(activity != null) {
            // iterate over all required permssion and chec
            permissionsRequired.forEach {
                allPermissionsAllowed = allPermissionsAllowed &&
                        (ContextCompat.checkSelfPermission(activity.applicationContext, it.value)
                        == PackageManager.PERMISSION_GRANTED)
            }
        }
        return allPermissionsAllowed
    }


    private fun requestPermissions() {
        val activity = activity
        if(activity != null) {

            permissionsRequired.forEach {
                val permissionIdNamePair = it.toPair()
                if (ContextCompat.checkSelfPermission(activity.applicationContext, permissionIdNamePair.second)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (shouldShowRequestPermissionRationale(permissionIdNamePair.second)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {

                    }
                    requestPermissions(
                            arrayOf(permissionIdNamePair.second),
                            permissionIdNamePair.first)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        for (index in permissions.indices) {
            val permissionName = permissions[index]
            val isGranted = grantResults[index]

            when(permissionName) {
                Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                    if(isGranted == PackageManager.PERMISSION_GRANTED) {
                        view!!.findViewById<CheckBox>(R.id.checkBoxStorage).isChecked = true
                    }
                }
            }
        }
    }
}

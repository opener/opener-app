package org.mytuc.etit.sse.openerapp.info

import android.content.Context
import android.net.Uri
import com.facebook.common.util.UriUtil
import java.io.File

/**
 * Created on 2019-08-05 by Authors:
 * <a href="mailto:thomas.graichen@etit.tu-chemnitz.de">Thomas Graichen</a>
 */
class AssetsUrlResolver(val context: Context?) : ImageUrlResolver {

    override
    fun resolveUris(imagesSrcDirPath: String) : List<Uri> {
        val uris = ArrayList<Uri>()
        val images = context?.assets?.list(imagesSrcDirPath)
        images?.forEach {

            val uri = Uri.Builder()
                    .scheme(UriUtil.LOCAL_ASSET_SCHEME)
                    .path(imagesSrcDirPath + File.separator + it)
                    .build()
            uris.add(uri)
        }
        return uris
    }

}
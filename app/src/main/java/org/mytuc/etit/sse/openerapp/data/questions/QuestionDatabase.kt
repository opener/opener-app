package org.mytuc.etit.sse.openerapp.data.questions

import org.mytuc.etit.sse.openerapp.R
import org.mytuc.etit.sse.openerapp.measure.MeasureMode
import org.mytuc.etit.sse.openerapp.measure.MeasureUnit

/**
 * This [QuestionDatabase] contains the DELFI-Questions. The used IDs can be found in the
 * DELFI-handbook (https://www.delfi.de/media/delfi_handbuch_barrierefreie_reiseketten_1._auflage_mai_2018.pdf)
 * at page 198 rsp. appendix page 38
 *
 */
object QuestionDatabase {


    val questions = listOf(

            // -------------------------------------------------------------------------------------
            // Stops
            // -------------------------------------------------------------------------------------

            //1010
            DescriptionQuestion(1010,
                    R.string.q_1010, R.string.q_1010_description,
                    listOf(Pair(R.string.description_example, "pics/1010/Example")),
                    R.string.q_1010_hint),

            //1011
            DescriptionQuestion(1011,
                    R.string.q_1011, R.string.q_1011_description,
                    listOf(Pair(R.string.description_example, "pics/1011/Example")),
                    R.string.q_1011_hint),

            // 1020
            ChoicesQuestion(1020,
                    R.string.q_1020, R.string.q_1020_description,
                    listOf(Pair(R.string.ans_yes, "pics/1020/Yes"),
                            Pair(R.string.ans_no, "pics/1020/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1021
            DescriptionQuestion(1021,
                    R.string.q_1021, R.string.q_1021_description,
                    listOf(Pair(R.string.description_example, "pics/1021/Example")),
                    R.string.q_1021_hint),

            // 1022
            ChoicesQuestion(1022,
                    R.string.q_1022, R.string.q_1022_description,
                    listOf(Pair(R.string.ans_yes, "pics/1022/Yes"),
                            Pair(R.string.ans_no, "pics/1022/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1030
            ChoicesQuestion(1030,
                    R.string.q_1030, R.string.q_1030_description,
                    listOf(Pair(R.string.ans_yes, "pics/1030/Yes"),
                            Pair(R.string.ans_no, "pics/1030/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1031
            DescriptionQuestion(1031,
                    R.string.q_1031, R.string.q_1031_description,
                    listOf(Pair(R.string.description_example, "pics/1031/Example")),
                    R.string.q_1031_hint),

            // 1032
            ChoicesQuestion(1032,
                    R.string.q_1032, R.string.q_1032_description,
                    listOf(Pair(R.string.ans_yes, "pics/1032/Yes"),
                            Pair(R.string.ans_no, "pics/1032/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1040
            ChoicesQuestion(1040,
                    R.string.q_1040, R.string.q_1040_description,
                    listOf(Pair(R.string.ans_yes, "pics/1040/Yes"),
                            Pair(R.string.ans_no, "pics/1040/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1050
            ChoicesQuestion(1050,
                    R.string.q_1050, R.string.q_1050_description,
                    listOf(Pair(R.string.ans_yes, "pics/1050/Yes"),
                            Pair(R.string.ans_no, "pics/1050/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1051
            DescriptionQuestion(1051,
                    R.string.q_1051, R.string.q_1051_description,
                    listOf(Pair(R.string.description_example, "pics/1051/Example")),
                    R.string.q_1051_hint),

            //1052
            DescriptionQuestion(1052,
                    R.string.q_1052, R.string.q_1052_description,
                    listOf(Pair(R.string.description_example, "pics/1052/Example")),
                    R.string.q_1052_hint),

            // 1060
            ChoicesQuestion(1060,
                    R.string.q_1060, R.string.q_1060_description,
                    listOf(Pair(R.string.ans_yes, "pics/1060/Yes"),
                            Pair(R.string.ans_no, "pics/1060/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1070
            ChoicesQuestion(1070,
                    R.string.q_1070, R.string.q_1070_description,
                    listOf(Pair(R.string.ans_yes, "pics/1070/Yes"),
                            Pair(R.string.ans_no, "pics/1070/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1071
            ChoicesQuestion(1071,
                    R.string.q_1071, R.string.q_1071_description,
                    listOf(Pair(R.string.ans_yes, "pics/1071/Yes"),
                            Pair(R.string.ans_no, "pics/1071/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1072
            ChoicesQuestion(1072,
                    R.string.q_1072, R.string.q_1072_description,
                    listOf(Pair(R.string.ans_yes, "pics/1072/Yes"),
                            Pair(R.string.ans_no, "pics/1072/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1073
            ChoicesQuestion(1073,
                    R.string.q_1073, R.string.q_1073_description,
                    listOf(Pair(R.string.ans_yes, "pics/1073/Yes"),
                            Pair(R.string.ans_no, "pics/1073/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1074
            ChoicesQuestion(1074,
                    R.string.q_1074, R.string.q_1074_description,
                    listOf(Pair(R.string.ans_yes, "pics/1074/Yes"),
                            Pair(R.string.ans_no, "pics/1074/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1075
            DescriptionQuestion(1075,
                    R.string.q_1075, R.string.q_1075_description,
                    listOf(Pair(R.string.description_example, "pics/1075/Example")),
                    R.string.q_1075_hint),

            // 1080
            ChoicesQuestion(1080,
                    R.string.q_1080, R.string.q_1080_description,
                    listOf(Pair(R.string.ans_yes, "pics/1080/Yes"),
                            Pair(R.string.ans_no, "pics/1080/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1081
            DescriptionQuestion(1081,
                    R.string.q_1081, R.string.q_1081_description,
                    listOf(Pair(R.string.description_example, "pics/1081/Example")),
                    R.string.q_1081_hint),

            //1082
            DescriptionQuestion(1082,
                    R.string.q_1082, R.string.q_1082_description,
                    listOf(Pair(R.string.description_example, "pics/1082/Example")),
                    R.string.q_1082_hint),

            // 1090
            ChoicesQuestion(1090,
                    R.string.q_1090, R.string.q_1090_description,
                    listOf(Pair(R.string.ans_yes, "pics/1090/Yes"),
                            Pair(R.string.ans_no, "pics/1090/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),
            // 1100
            ChoicesQuestion(1100,
                    R.string.q_1100, R.string.q_1100_description,
                    listOf(Pair(R.string.ans_yes, "pics/1100/Yes"),
                            Pair(R.string.ans_no, "pics/1100/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1110
            ChoicesQuestion(1110,
                    R.string.q_1110, R.string.q_1110_description,
                    listOf(Pair(R.string.ans_yes, "pics/1110/Yes"),
                            Pair(R.string.ans_no, "pics/1110/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1111
            ChoicesQuestion(1111,
                    R.string.q_1111, R.string.q_1111_description,
                    listOf(Pair(R.string.ans_yes, "pics/1111/Yes"),
                            Pair(R.string.ans_no, "pics/1111/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1112
            ChoicesQuestion(1112,
                    R.string.q_1112, R.string.q_1112_description,
                    listOf(Pair(R.string.ans_yes, "pics/1112/Yes"),
                            Pair(R.string.ans_no, "pics/1112/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1120
            ChoicesQuestion(1120,
                    R.string.q_1120, R.string.q_1120_description,
                    listOf(Pair(R.string.ans_yes, "pics/1120/Yes"),
                            Pair(R.string.ans_no, "pics/1120/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1130
            ChoicesQuestion(1130,
                    R.string.q_1130, R.string.q_1130_description,
                    listOf(Pair(R.string.ans_yes, "pics/1130/Yes"),
                            Pair(R.string.ans_no, "pics/1130/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1131
            ChoicesQuestion(1131,
                    R.string.q_1131, R.string.q_1131_description,
                    listOf(Pair(R.string.ans_yes, "pics/1131/Yes"),
                            Pair(R.string.ans_no, "pics/1131/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1140
            ChoicesQuestion(1140,
                    R.string.q_1140, R.string.q_1140_description,
                    listOf(Pair(R.string.ans_yes, "pics/1140/Yes"),
                            Pair(R.string.ans_no, "pics/1140/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1141
            ChoicesQuestion(1141,
                    R.string.q_1141, R.string.q_1141_description,
                    listOf(Pair(R.string.ans_yes, "pics/1141/Yes"),
                            Pair(R.string.ans_no, "pics/1141/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1150
            ChoicesQuestion(1150,
                    R.string.q_1150, R.string.q_1150_description,
                    listOf(Pair(R.string.ans_yes, "pics/1150/Yes"),
                            Pair(R.string.ans_no, "pics/1150/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1160
            ChoicesQuestion(1160,
                    R.string.q_1160, R.string.q_1160_description,
                    listOf(Pair(R.string.ans_yes, "pics/1160/Yes"),
                            Pair(R.string.ans_no, "pics/1160/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //1161
            DescriptionQuestion(1161,
                    R.string.q_1161, R.string.q_1161_description,
                    listOf(Pair(R.string.description_example, "pics/1161/Example")),
                    R.string.q_1161_hint),

            //1170
            MeasurementQuestion(1170,
                    R.string.q_1170, R.string.q_1170_description,
                    listOf(Pair(R.string.description_example, "pics/1170/Example")), //TODO: Pfad zu Beispielbildern hinzufügen
                    MeasureUnit.CM,
                    MeasureMode.ELEVATION),

            //1180
            MeasurementQuestion(1180,
                    R.string.q_1180, R.string.q_1180_description,
                    listOf(Pair(R.string.description_example, "pics/1180/Example")), //TODO: Pfad zu Beispielbildern hinzufügen
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            //1190
            MeasurementQuestion(1190,
                    R.string.q_1190, R.string.q_1190_description,
                    listOf(Pair(R.string.description_example, "pics/1190/Example")), //TODO: Pfad zu Beispielbildern hinzufügen
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 1200
            ChoicesQuestion(1200,
                    R.string.q_1200, R.string.q_1200_description,
                    listOf(Pair(R.string.ans_yes, "pics/1200/Yes"),
                            Pair(R.string.ans_no, "pics/1200/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1201
            ChoicesQuestion(1201,
                    R.string.q_1201, R.string.q_1201_description,
                    listOf(Pair(R.string.ans_yes, "pics/1201/Yes"),
                            Pair(R.string.ans_no, "pics/1201/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1202
            ChoicesQuestion(1202,
                    R.string.q_1202, R.string.q_1202_description,
                    listOf(Pair(R.string.ans_yes, "pics/1202/Yes"),
                            Pair(R.string.ans_no, "pics/1202/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1203
            ChoicesQuestion(1203,
                    R.string.q_1203, R.string.q_1203_description,
                    listOf(Pair(R.string.ans_yes, "pics/1203/Yes"),
                            Pair(R.string.ans_no, "pics/1203/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1210
            ChoicesQuestion(1210,
                    R.string.q_1210, R.string.q_1210_description,
                    listOf(Pair(R.string.ans_yes, "pics/1210/Yes"),
                            Pair(R.string.ans_no, "pics/1210/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1211
            MeasurementQuestion(1211,
                    R.string.q_1211, R.string.q_1211_description,
                    listOf(Pair(R.string.description_example, "pics/1211/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 1212
            MeasurementQuestion(1212,
                    R.string.q_1212, R.string.q_1212_description,
                    listOf(Pair(R.string.description_example, "pics/1212/Example")),
                    MeasureUnit.KG,
                    MeasureMode.NOTHING),

            // 1220
            ChoicesQuestion(1220,
                    R.string.q_1220, R.string.q_1220_description,
                    listOf(Pair(R.string.ans_yes, "pics/1220/Yes"),
                            Pair(R.string.ans_no, "pics/1220/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 1221
            MeasurementQuestion(1221,
                    R.string.q_1221, R.string.q_1221_description,
                    listOf(Pair(R.string.description_example, "pics/1221/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 1222
            MeasurementQuestion(1222,
                    R.string.q_1222, R.string.q_1222_description,
                    listOf(Pair(R.string.description_example, "pics/1222/Example")),
                    MeasureUnit.KG,
                    MeasureMode.NOTHING),

            // -------------------------------------------------------------------------------------
            // Ways
            // -------------------------------------------------------------------------------------

            // 2010
            ChoicesQuestion(2010,
                    R.string.q_2010, R.string.q_2010_description,
                    listOf(Pair(R.string.ans_yes, "pics/2010/Yes"),
                            Pair(R.string.ans_no, "pics/2010/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2020
            MeasurementQuestion(2020,
                    R.string.q_2020, R.string.q_2020_description,
                    listOf(Pair(R.string.description_example, "pics/2020/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),
            // 2021
            MeasurementQuestion(2021,
                    R.string.q_2021, R.string.q_2021_description,
                    listOf(Pair(R.string.description_example, "pics/2021/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2030
            ChoicesQuestion(2030,
                    R.string.q_2030, R.string.q_2030_description,
                    listOf(Pair(R.string.ans_yes, "pics/2030/Yes"),
                            Pair(R.string.ans_no, "pics/2030/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //2031
            DescriptionQuestion(2031,
                    R.string.q_2031, R.string.q_2031_description,
                    listOf(Pair(R.string.description_example, "pics/2031/Example")),
                    R.string.q_2031_hint),

            // 2032
            ChoicesQuestion(2032,
                    R.string.q_2032, R.string.q_2032_description,
                    listOf(Pair(R.string.ans_Drehtuer, "pics/2032/Dreh"),
                            Pair(R.string.ans_Pendeltuer, "pics/2032/Pendel"),
                            Pair(R.string.ans_Rotationstuer, "pics/2032/Rotation"),
                            Pair(R.string.ans_Schiebetuer, "pics/2032/Schiebe")/*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2033
            ChoicesQuestion(2033,
                    R.string.q_2033, R.string.q_2033_description,
                    listOf(Pair(R.string.ans_auto, "pics/2033/automatisch"),
                            Pair(R.string.ans_halbauto, "pics/2033/halbautomatisch"),
                            Pair(R.string.ans_manuell, "pics/2033/manuell")/*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2034
            MeasurementQuestion(2034,
                    R.string.q_2034, R.string.q_2034_description,
                    listOf(Pair(R.string.description_example, "pics/2034/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2040
            ChoicesQuestion(2040,
                    R.string.q_2040, R.string.q_2040_description,
                    listOf(Pair(R.string.ans_yes, "pics/2040/Yes"),
                            Pair(R.string.ans_no, "pics/2040/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2050
            ChoicesQuestion(2050,
                    R.string.q_2050, R.string.q_2050_description,
                    listOf(Pair(R.string.ans_yes, "pics/2050/Yes"),
                            Pair(R.string.ans_no, "pics/2050/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //2060
            DescriptionQuestion(2060,
                    R.string.q_2060, R.string.q_2060_description,
                    listOf(Pair(R.string.description_example, "pics/2060/Example")),
                    R.string.q_2060_hint),

            // 2070
            ChoicesQuestion(2070,
                    R.string.q_2070, R.string.q_2070_description,
                    listOf(Pair(R.string.ans_yes, "pics/2070/Yes"),
                            Pair(R.string.ans_no, "pics/2070/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2071
            ChoicesQuestion(2071,
                    R.string.q_2071, R.string.q_2071_description,
                    listOf(Pair(R.string.ans_yes, "pics/2071/Yes"),
                            Pair(R.string.ans_no, "pics/2071/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2072
            ChoicesQuestion(2072,
                    R.string.q_2072, R.string.q_2072_description,
                    listOf(Pair(R.string.ans_yes, "pics/2072/Yes"),
                            Pair(R.string.ans_no, "pics/2072/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2080
            MeasurementQuestion(2080,
                    R.string.q_2080, R.string.q_2080_description,
                    listOf(Pair(R.string.description_example, "pics/2080/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2081
            MeasurementQuestion(2081,
                    R.string.q_2081, R.string.q_2081_description,
                    listOf(Pair(R.string.description_example, "pics/2081/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2090
            ChoicesQuestion(2090,
                    R.string.q_2090, R.string.q_2090_description,
                    listOf(Pair(R.string.ans_yes, "pics/2090/Yes"),
                            Pair(R.string.ans_no, "pics/2090/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2091
            MeasurementQuestion(2091,
                    R.string.q_2091, R.string.q_2091_description,
                    listOf(Pair(R.string.description_example, "pics/2091/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2092
            MeasurementQuestion(2092,
                    R.string.q_2092, R.string.q_2092_description,
                    listOf(Pair(R.string.description_example, "pics/2092/Example")),
                    MeasureUnit.M2,
                    MeasureMode.NOTHING),

            // 2093
            MeasurementQuestion(2093,
                    R.string.q_2093, R.string.q_2093_description,
                    listOf(Pair(R.string.description_example, "pics/2093/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2094
            MeasurementQuestion(2094,
                    R.string.q_2094, R.string.q_2094_description,
                    listOf(Pair(R.string.description_example, "pics/2094/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2095
            MeasurementQuestion(2095,
                    R.string.q_2095, R.string.q_2095_description,
                    listOf(Pair(R.string.description_example, "pics/2095/Example")),
                    MeasureUnit.NUM,
                    MeasureMode.NOTHING),

            // 2100
            ChoicesQuestion(2100,
                    R.string.q_2100, R.string.q_2100_description,
                    listOf(Pair(R.string.ans_yes, "pics/2100/Yes"),
                            Pair(R.string.ans_no, "pics/2100/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2101
            MeasurementQuestion(2101,
                    R.string.q_2101, R.string.q_2101_description,
                    listOf(Pair(R.string.description_example, "pics/2101/Example")),
                    MeasureUnit.CM,
                    MeasureMode.ELEVATION),

            // 2110
            ChoicesQuestion(2110,
                    R.string.q_2110, R.string.q_2110_description,
                    listOf(Pair(R.string.ans_yes, "pics/2110/Yes"),
                            Pair(R.string.ans_no, "pics/2110/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //2111
            DescriptionQuestion(2111,
                    R.string.q_2111, R.string.q_2111_description,
                    listOf(Pair(R.string.description_example, "pics/2111/Example")),
                    R.string.q_2111_hint),

            // 2112
            MeasurementQuestion(2112,
                    R.string.q_2112, R.string.q_2112_description,
                    listOf(Pair(R.string.description_example, "pics/2112/Example")),
                    MeasureUnit.CM,
                    MeasureMode.ELEVATION),

            // 2113
            MeasurementQuestion(2113,
                    R.string.q_2113, R.string.q_2113_description,
                    listOf(Pair(R.string.description_example, "pics/2113/Example")),
                    MeasureUnit.NUM,
                    MeasureMode.NOTHING),

            // 2120
            ChoicesQuestion(2120,
                    R.string.q_2120, R.string.q_2120_description,
                    listOf(Pair(R.string.ans_yes, "pics/2120/Yes"),
                            Pair(R.string.ans_no, "pics/2120/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //2121
            DescriptionQuestion(2121,
                    R.string.q_2121, R.string.q_2121_description,
                    listOf(Pair(R.string.description_example, "pics/2121/Example")),
                    R.string.q_2121_hint),

            // 2122
            MeasurementQuestion(2122,
                    R.string.q_2122, R.string.q_2122_description,
                    listOf(Pair(R.string.description_example, "pics/2122/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2123
            MeasurementQuestion(2123,
                    R.string.q_2123, R.string.q_2123_description,
                    listOf(Pair(R.string.description_example, "pics/2123/Example")),
                    MeasureUnit.CM,
                    MeasureMode.DISTANCE),

            // 2124
            MeasurementQuestion(2124,
                    R.string.q_2124, R.string.q_2124_description,
                    listOf(Pair(R.string.description_example, "pics/2124/Example")),
                    MeasureUnit.PERCENT,
                    MeasureMode.NOTHING),

            // 2130
            ChoicesQuestion(2130,
                    R.string.q_2130, R.string.q_2130_description,
                    listOf(Pair(R.string.ans_yes, "pics/2130/Yes"),
                            Pair(R.string.ans_no, "pics/2130/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            //2131
            DescriptionQuestion(2131,
                    R.string.q_2131, R.string.q_2131_description,
                    listOf(Pair(R.string.description_example, "pics/2131/Example")),
                    R.string.q_2131_hint),

            // 2132
            ChoicesQuestion(2132,
                    R.string.q_2132, R.string.q_2132_description,
                    listOf(Pair(R.string.ans_up, "pics/2132/Yes"),
                            Pair(R.string.ans_down, "pics/2132/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2133
            ChoicesQuestion(2133,
                    R.string.q_2133, R.string.q_2133_description,
                    listOf(Pair(R.string.ans_yes, "pics/2133/Yes"),
                            Pair(R.string.ans_no, "pics/2133/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            ),

            // 2134
            MeasurementQuestion(2134,
                    R.string.q_2134, R.string.q_2134_description,
                    listOf(Pair(R.string.description_example, "pics/2134/Example")),
                    MeasureUnit.SEC,
                    MeasureMode.NOTHING),

            // 2140
            ChoicesQuestion(2140,
                    R.string.q_2140, R.string.q_2140_description,
                    listOf(Pair(R.string.ans_yes, "pics/2140/Yes"),
                            Pair(R.string.ans_no, "pics/2140/No") /*,
                            Pair(R.string.ans_dontknow, "pics/pavement/unpaved") just for testing the dynamic layout */
                    )
            )

    )
}
package org.mytuc.etit.sse.openerapp.measure


import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color.parseColor
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import com.google.android.material.snackbar.Snackbar
import com.google.ar.core.*
import com.google.ar.core.exceptions.UnavailableException
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.*
import kotlinx.android.synthetic.main.fragment_measure.*
import org.mytuc.etit.sse.openerapp.R
import java.util.concurrent.CompletableFuture
import kotlin.math.sqrt
import android.view.MotionEvent
import android.view.MotionEvent.INVALID_POINTER_ID
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import com.google.ar.sceneform.HitTestResult
import com.google.ar.sceneform.collision.Box
import com.google.ar.sceneform.ux.*
import org.mytuc.etit.sse.openerapp.model.OpenerViewModel
import kotlin.math.pow


/**
 * Fragment for measurement feature
 * Author: Anusha Achutan
 *
 */
@Suppress("CAST_NEVER_SUCCEEDS")
class MeasureFragment : ArFragment() {

    private val fragmentTag: String = "MeasureModuleDebug"
    private val minOpenGLVersion: Double = 3.0

    // Enable/ Disable debug prints
    private val debugPrintsEnable = false
    private lateinit var debugText: String

    private lateinit var mMode: MeasureMode
    private var questionID: Int = 0

    private lateinit var tvMisc: TextView
    private var loadingMessageSnackbar: Snackbar? = null
    private var otherMessageSnackbar: Snackbar? = null

    // For vertical plane detection
    private var planeVerticalflag: Boolean = false
    private var distanceRenderablePlaced: Boolean = false
    private var elevationRenderablePlaced: Boolean = false

    // Current frame in the scene, updated whenever the scene is updated
    private lateinit var curFrame: Frame

    // Initialize Anchor and Anchor Nodes
    private var anchorA: Anchor? = null
    private var anchorB: Anchor? = null
    private var anchorNodeA: AnchorNode? = null
    private var anchorNodeB: AnchorNode? = null

    // Node where the renderable is placed
    private var destNode = Node()
    private var childNodeA = Node()
    private var childNodeB = Node()
    private var hTestDistance: Float = 0.0f

    // Initialize renderables
    private lateinit var mRenderable: CompletableFuture<Material>
    private lateinit var mRenderableSV: CompletableFuture<Material>
    private lateinit var mRenderableSVB: CompletableFuture<Material>
    private lateinit var lineRenderableTexture: CompletableFuture<Texture>
    private var hRenderable: ModelRenderable? = null
    private var lRenderable: ModelRenderable? = null
    private var sRenderable: ModelRenderable? = null
    private var sRenderableB: ModelRenderable? = null

    // Output
    private var finalElevation: Int = 0
    private var finalDistance: Int = 0

    // Place model in the beginning
    private lateinit var aInitialPoint: android.graphics.Point
    private lateinit var bInitialPoint: android.graphics.Point

    //Pointers for user movement tracking
    private var mTouchX:Float = 0.0f
    private var mTouchY:Float = 0.0f
    private var mActivePointerId: Int = 0
    private var selectedMovableNode: Int = 0


    private lateinit var model: OpenerViewModel

    // Check if device is supported: SDK Version => 24, OpenGL >= 3.0
    override fun onCreate(savedInstanceState: Bundle?) {
        // load fragment data
        model = activity?.run {
            ViewModelProvider(this).get(OpenerViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


        if (!checkIsSupportedDeviceOrFinish(this)) {
            return
        }
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Create Renderable
        val res = context.resources
        val id = R.drawable.ic_gyellow_texture
        lineRenderableTexture = Texture.builder().setSource(BitmapFactory.decodeResource(res,id)).build()
        lineRenderableTexture.thenAccept {
            mRenderable = MaterialFactory.makeOpaqueWithTexture(context, it)
        }
        // Create custom selection visualizer renderable for distance renderable
        mRenderableSV = MaterialFactory.makeOpaqueWithColor(context, Color(255f,0f,0f))
        mRenderableSVB = MaterialFactory.makeOpaqueWithColor(context, Color(0f,0f,255f))
    }

    // On fragment pause: clear measurement, text views and snackbars
    override fun onPause() {
        super.onPause()
        clearMeasurements()
        text_ViewInstructions.text = null
        textViewMeasureResult.text = null
        textViewMeasureResult.setBackgroundColor(0)
        loadingMessageSnackbar?.dismiss()
        otherMessageSnackbar?.dismiss()
        removeMeasurementButton.hide()
        confirmMeasurementButton.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // FAB for measurement completed
        confirmMeasurementButton.setOnClickListener {
            // Returns back to previous fragment
            when(mMode) {
                MeasureMode.DISTANCE -> {
                    if(finalDistance != 0 && questionID != 0) {
                        model.answers[questionID] = finalDistance.toString()
                    }
                }
                MeasureMode.ELEVATION -> {
                    if(finalElevation != 0 && questionID != 0) {
                        model.answers[questionID] = finalElevation.toString()
                    }
                }
            }
            activity!!.onBackPressed()
        }
        confirmMeasurementButton.hide()

        // FAB for clear measurement
        removeMeasurementButton.setOnClickListener { view ->
            clearMeasurements()
            text_ViewInstructions.text = null
            otherMessageSnackbar = Snackbar.make(view, "All measurements cleared", Snackbar.LENGTH_LONG)
            removeMeasurementButton.hide()
            confirmMeasurementButton.hide()
            // Show dragabble view
            if(mMode == MeasureMode.DISTANCE)
            {
                tvMisc.text = resources.getString(R.string.ar_distance_measurement)
                imageViewAR.setImageResource(R.drawable.ic_distance_renderable)
            }
            if(mMode == MeasureMode.ELEVATION)
            {
                tvMisc.text = resources.getString(R.string.ar_height_measurement)
                imageViewAR.setImageResource(R.drawable.ic_elevation_renderable)
            }
            imageViewAR.visibility = View.VISIBLE
            imageViewAR.setBackgroundColor(parseColor("#66141414"))
        }
        removeMeasurementButton.hide()

    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_measure, container, false)

        val argumentsSafe = arguments
        val fragmentArgs = argumentsSafe?.let { MeasureFragmentArgs.fromBundle(it) }


        // Initialise text view with arguments
        tvMisc = rootView.findViewById(R.id.textViewMeasureResult)
        tvMisc.setBackgroundColor(parseColor("#66141414"))
        if (fragmentArgs != null) {
            mMode = fragmentArgs.measureMode
            questionID = fragmentArgs.delfiID
        }

        val imageView = rootView.findViewById<ImageView>(R.id.imageViewAR)
        imageView.setOnTouchListener{ thisView: View, motionEvent: MotionEvent ->
            imageViewOnTouchListener(thisView,motionEvent)
        }
        imageView.visibility = View.INVISIBLE

        // Get AR Container
        val arContainer = rootView.findViewById<FrameLayout>(R.id.ar_container)
        // Call the ArFragment's implementation to get the AR View.
        val arView = super.onCreateView(inflater, arContainer, savedInstanceState)
        arContainer.addView(arView)

        // Show loading snackbar while waiting on plane
        showLoadingMessage(R.string.ar_loading_instructions)

        // Wait till plane is detected
        arSceneView.scene.addOnUpdateListener {
            val planeRenderer = arSceneView.planeRenderer
            planeRenderer.material.thenAccept {
                it.setFloat(PlaneRenderer.MATERIAL_SPOTLIGHT_RADIUS, 5000f)
            }
            planeRenderer.isShadowReceiver = false
            planeRenderer.isShadowReceiver = false
            arSceneView.scene.camera.farClipPlane = 800.0f // 800m

            // Get current frame and show draggable view
            curFrame = arSceneView.arFrame ?: return@addOnUpdateListener
            for(plane in  curFrame.getUpdatedTrackables(Plane::class.java) )
            {
                if (plane.trackingState == TrackingState.TRACKING) {
                    // Plane is found, hide the snackbar message
                    hideLoadingMessage()
                    planeDiscoveryController.hide()
                    if(mMode == MeasureMode.DISTANCE && !distanceRenderablePlaced)
                    {
                        imageView.visibility = View.VISIBLE
                        imageView.setImageResource(R.drawable.ic_distance_renderable)
                        imageView.setBackgroundColor(parseColor("#66141414"))
                    }
                    if(mMode == MeasureMode.ELEVATION && !elevationRenderablePlaced)
                    {
                        imageView.visibility = View.VISIBLE
                        imageView.setImageResource(R.drawable.ic_elevation_renderable)
                        imageView.setBackgroundColor(parseColor("#66141414"))
                    }
                }
                else
                {
                    debugText = "Tracking Stopped/Paused"
                    writeToLog(fragmentTag,debugText)
                }
            }
        }

        arSceneView.scene.setOnTouchListener { _, _ ->
            return@setOnTouchListener true
        }

        setOnTapArPlaneListener { _, _, _ ->
            return@setOnTapArPlaneListener
        }

        // Listener for draggable view
        arContainer.setOnDragListener { _, event ->
            onImageDrag(event)
        }

        // Listener to handle user touch events for distance mode
        if(mMode == MeasureMode.DISTANCE) {
            tvMisc.text = resources.getString(R.string.ar_distance_measurement)
            arSceneView.scene.addOnPeekTouchListener{ _: HitTestResult, event:MotionEvent->
                // For debugging
                val action = event.action
                val actionMask = event.actionMasked
                debugText = String.format("Action: %s, Action Mask: %s",MotionEvent.actionToString(action),actionMask.toString())
                writeToLog(fragmentTag,debugText)
                debugText = String.format("x: %.2f, y: %.2f",event.getX(0),event.getY(0))
                writeToLog(fragmentTag,debugText)
                if (distanceRenderablePlaced) {
                    onUserTouchEventDistance(event)
                }
                return@addOnPeekTouchListener
            }
        }
        else {
            tvMisc.text = resources.getString(R.string.ar_height_measurement)
            arSceneView.scene.addOnPeekTouchListener { _: HitTestResult, _: MotionEvent ->
                return@addOnPeekTouchListener
            }
        }
        return rootView
    }

    // Image drag and drop
    private fun imageViewOnTouchListener(thisView: View, motionEvent: MotionEvent) : Boolean
    {
        val action = motionEvent.action
        thisView.setBackgroundColor(0)
        if(action == MotionEvent.ACTION_DOWN)
        {
            // Construct draggable shadow for view
            val shadowBuilder = View.DragShadowBuilder(thisView)
            // Start the drag of the shadow
            thisView.startDragAndDrop(null, shadowBuilder, thisView, 0)
            // Hide the actual view as shadow is being dragged
            thisView.visibility = View.INVISIBLE
        }
        else
        {
            return false
        }
        return true
    }

    // Image drag and drop
    private fun onImageDrag(dragEvent: DragEvent) : Boolean
    {
        when (dragEvent.action)
        {
            DragEvent.ACTION_DROP -> {
                // Place Distance/ Elevation Renderable
                if(mMode == MeasureMode.DISTANCE && !distanceRenderablePlaced)
                {
                    placeDistanceRenderable(dragEvent)
                }
                if(mMode == MeasureMode.ELEVATION && !elevationRenderablePlaced)
                {
                    placeElevationRenderable(dragEvent)
                }
                val dropLocationX = dragEvent.x
                val dropLocationY = dragEvent.y
                imageViewAR.visibility = View.INVISIBLE
                debugText = String.format("Dropping at %.2f and %.2f",dropLocationX,dropLocationY)
                writeToLog(fragmentTag,debugText)
            }
        }
        return true
    }

    // Handle user drag for distance mode
    private fun onUserTouchEventDistance(motionEvent: MotionEvent) : Boolean
    {
        val action = motionEvent.action
        when (action)
        {
            MotionEvent.ACTION_DOWN ->
            {
                motionEvent.actionMasked.also { pointerIndex ->
                    // Remember where we started (for dragging)
                    mTouchX = motionEvent.getX(pointerIndex)
                    mTouchY = motionEvent.getY(pointerIndex)
                }

                // Save the ID of this pointer (for dragging)
                mActivePointerId = motionEvent.getPointerId(0)

                // Screen points x,y are between 0 and SceneWidth if the point is in the current scene
                // Extract the current world coordinates and convert to screen points for comparision
                val screenLocationA = arSceneView.scene.camera.worldToScreenPoint(anchorNodeA!!.worldPosition)
                val screenLocationB = arSceneView.scene.camera.worldToScreenPoint(anchorNodeB!!.worldPosition)
                val sPointA: android.graphics.Point = android.graphics.Point(screenLocationA.x.toInt(),screenLocationA.y.toInt())
                val sPointB: android.graphics.Point = android.graphics.Point(screenLocationB.x.toInt(),screenLocationB.y.toInt())
                if((sPointA.x in 0..arSceneView.width) && (sPointA.y in 0..arSceneView.height))
                {
                    // For debugging
                    debugText = String.format("A in view, sPointA (%d,%d)",sPointA.x,sPointA.y)
                    writeToLog(fragmentTag,debugText)
                }
                if((sPointB.x in 0..arSceneView.width) && (sPointB.y in 0..arSceneView.height))
                {
                    // For debugging
                    debugText = String.format("B in view, sPointB (%d,%d)",sPointB.x,sPointB.y)
                    writeToLog(fragmentTag,debugText)
                }

                val diffXA = screenLocationA.x - mTouchX
                val diffYA = screenLocationA.y - mTouchY
                val diffXB = screenLocationB.x - mTouchX
                val diffYB = screenLocationB.y - mTouchY

                // For debugging
                debugText = String.format("Touch Coordinates (%.2f,%.2f)",mTouchX,mTouchY)
                writeToLog(fragmentTag,debugText)
                debugText = String.format("diffA (%.2f, %.2f), diffB (%.2f, %.2f)",diffXA,diffYA,diffXB,diffYB)
                writeToLog(fragmentTag,debugText)

                // If touch is near node A then move node A. Check if the touch point is near A or B and set flag
                val diffA = sqrt(diffXA.pow(2) + diffYA.pow(2))
                val metrics: DisplayMetrics = resources.displayMetrics
                val inRangeA = diffA < (metrics.xdpi/2.54) * 2.0 //2.0 cm
                val diffB = sqrt(diffXB.pow(2) + diffYB.pow(2))
                val inRangeB = diffB < (metrics.xdpi/2.54) * 2.0 //2.0 cm
                debugText = String.format("inRangeA: %s, inRangeB: %s",inRangeA,inRangeB)
                writeToLog(fragmentTag,debugText)

                if(inRangeA)
                {
                    debugText = String.format("In range and view of A")
                    writeToLog(fragmentTag, debugText)
                    selectedMovableNode = 1
                }
                else if(inRangeB)
                {
                    debugText = String.format("In range and view of B")
                    writeToLog(fragmentTag, debugText)
                    selectedMovableNode = 2
                }

                // If not near A or B / near both A and B -> Select the nearest
                if((inRangeA && inRangeB) || (!inRangeA && !inRangeB))
                {
                    selectedMovableNode = when {
                        diffA < diffB -> 1
                        diffB < diffA -> 2
                        else -> 2
                    }
                }
            }

            MotionEvent.ACTION_MOVE ->
            {
                // If user dragged, resize distance renderable
                if(distanceRenderablePlaced) {
                    onUserDragEventDistance(motionEvent)
                    debugText = String.format("User drag performed")
                    writeToLog(fragmentTag,debugText)
                }
                mTouchX = 0.0f
                mTouchY = 0.0f
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL ->
            {
                mActivePointerId = INVALID_POINTER_ID
                debugText = String.format("-----------------------------------------------------------")
                writeToLog(fragmentTag,debugText)
                // Only receive the next down
                return false
            }
            MotionEvent.ACTION_POINTER_UP ->
            {
                // Update pointer if another finger touches the screen
                motionEvent.actionIndex.also { pointerIndex ->
                    motionEvent.getPointerId(pointerIndex).takeIf { it == mActivePointerId }?.run {
                        // This was our active pointer going up. Choose a new
                        // active pointer and adjust accordingly.
                        val newPointerIndex = if (pointerIndex == 0) 1 else 0
                        mTouchX = motionEvent.getX(newPointerIndex)
                        mTouchY = motionEvent.getY(newPointerIndex)
                        mActivePointerId = motionEvent.getPointerId(newPointerIndex)
                    }
                }
            }
        }
        return true
    }

    // Resize distance renderable when user drags
    private fun onUserDragEventDistance(motionEvent: MotionEvent): Boolean
    {
        // Extract X,Y coordinates and perform hittest on the current frame
        val xaxisVal = motionEvent.getAxisValue(MotionEvent.AXIS_X, motionEvent.getPointerId(motionEvent.pointerCount - 1))
        val yaxisVal = motionEvent.getAxisValue(MotionEvent.AXIS_Y, motionEvent.getPointerId(motionEvent.pointerCount - 1))
        val aResult = curFrame.hitTest(xaxisVal, yaxisVal)
        for (aHit in aResult) {
            val aTrackable = aHit.trackable
            if (aTrackable is Plane && (aTrackable.isPoseInPolygon(aHit.hitPose))) {
                if(selectedMovableNode == 1)
                {
                    debugText = String.format("A is changed, selectedMovableNode: %s",selectedMovableNode)
                    writeToLog(fragmentTag,debugText)
                    anchorA = aHit.createAnchor()
                    anchorNodeA = AnchorNode(anchorA)
                    anchorNodeA!!.setParent(arSceneView.scene)
                    measureDistance()

                    /*
                    // Debugging
                    hTestDistance = aHit.distance
                    text_ViewDebug.text = String.format("HRD %.2f",hTestDistance)
                    text_ViewDebug.setBackgroundColor(parseColor("#66141414"))*/
                }
                else if(selectedMovableNode == 2)
                {
                    debugText = String.format("B is changed, selectedMovableNode: %s",selectedMovableNode)
                    writeToLog(fragmentTag,debugText)
                    anchorB = aHit.createAnchor()
                    anchorNodeB = AnchorNode(anchorB)
                    anchorNodeB!!.setParent(arSceneView.scene)
                    measureDistance()

                    // Debugging
                    /*
                    hTestDistance = aHit.distance
                    text_ViewDebug.text = String.format("HRD %.2f",hTestDistance)
                    text_ViewDebug.setBackgroundColor(parseColor("#66141414"))*/
                }
                break
            }
        }
        return true
    }

    // Place distance renderable
    private fun placeDistanceRenderable(dragEvent: DragEvent)
    {
        // Create custom footprint selection visualizer
        mRenderableSV.thenAccept { material ->
            sRenderable = ShapeFactory.makeSphere(0.01f,Vector3.zero(),material)
            //ShapeFactory.makeCylinder(0.005f,0.0001f,Vector3.zero(),material)
            sRenderable?.isShadowCaster = false
            sRenderable?.isShadowReceiver = false
        }
        mRenderableSVB.thenAccept { material ->
            sRenderableB = ShapeFactory.makeSphere(0.01f,Vector3.zero(),material)
            //ShapeFactory.makeCylinder(0.005f,0.0001f,Vector3.zero(),material)
            sRenderableB?.isShadowCaster = false
            sRenderableB?.isShadowReceiver = false
        }

        // Place renderable at user image drag event and fixed initial points
        aInitialPoint = android.graphics.Point(dragEvent.x.toInt() - 150 ,dragEvent.y.toInt() - 150)
        //aInitialPoint = android.graphics.Point(view!!.width / 2, view!!.height / 2)
        bInitialPoint = android.graphics.Point(aInitialPoint.x + 300, aInitialPoint.y + 300)
        // Perform a hit test by projecting a ray to the center of the screen
        val aResult = curFrame.hitTest(aInitialPoint.x.toFloat(), aInitialPoint.y.toFloat())
        for (aHit in aResult) {
            val aTrackable = aHit.trackable
            if (aTrackable is Plane && (aTrackable.isPoseInPolygon(aHit.hitPose))) {
                val bResult = curFrame.hitTest(bInitialPoint.x.toFloat(), bInitialPoint.y.toFloat())
                for (bHit in bResult) {
                    val trackable2 = bHit.trackable
                    if (trackable2 is Plane && trackable2.isPoseInPolygon(bHit.hitPose)) {
                        anchorA = aHit.createAnchor()
                        anchorNodeA = AnchorNode(anchorA)
                        anchorNodeA!!.setParent(arSceneView.scene)
                        anchorB = bHit.createAnchor()
                        anchorNodeB = AnchorNode(anchorB)
                        anchorNodeB!!.setParent(arSceneView.scene)
                        measureDistance()

                        /*
                        // Debugging
                        hTestDistance = aHit.distance
                        text_ViewDebug.text = String.format("HRD %.2f",hTestDistance)
                        text_ViewDebug.setBackgroundColor(parseColor("#66141414"))*/
                        break
                    }
                    else
                    {
                        // If user tries to place renderable at non-plane position
                        imageViewAR.visibility = View.VISIBLE
                        imageViewAR.setImageResource(R.drawable.ic_distance_renderable)
                        imageViewAR.setBackgroundColor(parseColor("#66141414"))
                    }
                }
            }
            else
            {
                // If user tries to place renderable at non-plane position
                imageViewAR.visibility = View.VISIBLE
                imageViewAR.setImageResource(R.drawable.ic_distance_renderable)
                imageViewAR.setBackgroundColor(parseColor("#66141414"))

            }
        }
    }

    // Create renderable between anchorA and anchorB and measure distance
    private fun measureDistance()
    {
        val nodeA = Node()
        nodeA.setParent(null)
        nodeA.setParent(anchorNodeA)
        // Place red sphere at the end of line
        childNodeA.setParent(nodeA)
        childNodeA.renderable = sRenderable

        val nodeB =  Node()
        nodeB.setParent(anchorNodeB)
        // Place red sphere at the other end of line
        childNodeB.setParent(nodeB)
        if(debugPrintsEnable)
        {
            // Blue sphere for debug mode
            childNodeB.renderable = sRenderableB
        }
        else
        {
            childNodeB.renderable = sRenderable
        }

        val point1: Vector3 = anchorNodeA!!.worldPosition
        val point2: Vector3 = anchorNodeB!!.worldPosition

        val difference = Vector3.subtract(point1, point2)

        finalDistance = calculateDistance(point1,point2).toInt()
        val distanceString = resources.getString(R.string.distance)
        val text = resources.getString(R.string.measure_result,distanceString, finalDistance)
        textViewMeasureResult.text = text
        textViewMeasureResult.setBackgroundColor(parseColor("#66141414"))

        val directionFromTopToBottom = difference.normalized()
        val rotationFromAToB = Quaternion.lookRotation(directionFromTopToBottom, Vector3.up())
        mRenderable.thenAccept { material ->
            lRenderable =
                    ShapeFactory.makeCylinder(0.0075f,difference.length(),Vector3.zero(),material)
            //ShapeFactory.makeCube(Vector3(.01f, .001f, difference.length()), Vector3.zero(), material)
            lRenderable?.isShadowReceiver = false
            lRenderable?.isShadowCaster = false
        }

        if(selectedMovableNode == 1)
        {
            // TA can be moved an TB stays fixed
            destNode.setParent(nodeB)
        }
        else if(selectedMovableNode == 2)
        {
            // TB can be moved and TA stays fixed
            destNode.setParent(nodeA)
        }
        else
        {
            // TB can be moved and TA stays fixed
            destNode.setParent(nodeA)
        }

        destNode.renderable = lRenderable
        distanceRenderablePlaced = true
        confirmMeasurementButton.show()
        removeMeasurementButton.show()
        text_ViewInstructions.text = getString(R.string.ar_lRenderable_instructions)
        // destNode World position is the mid point between point 1 and 2
        destNode.worldPosition = Vector3.add(point1, point2).scaled(.5f)
        //destNode.worldRotation = rotationFromAToB
        destNode.worldRotation = Quaternion.multiply(rotationFromAToB, Quaternion.axisAngle(Vector3(1.0f, 0.0f, 0.0f), 90F))
    }

    // Place elevation renderable
    private fun placeElevationRenderable(dragEvent: DragEvent)
    {
        mRenderableSV.thenAccept { material ->
            sRenderable = //ShapeFactory.makeSphere(0.01f,Vector3.zero(),material)
                    ShapeFactory.makeCylinder(0.02f,0.0005f,Vector3.zero(),material)
            sRenderable?.isShadowCaster = false
            sRenderable?.isShadowReceiver = false
        }
        // Place renderable at user image drag event
        //aInitialPoint = android.graphics.Point(view!!.width / 2, view!!.height / 2)
        aInitialPoint = android.graphics.Point(dragEvent.x.toInt(), dragEvent.y.toInt())
        // Perform a hit test by projecting a ray to the center of the screen
        val aResult = curFrame.hitTest(aInitialPoint.x.toFloat(), aInitialPoint.y.toFloat() + 75)
        // Return the deepest hit result. Currently gives a sporadic behaviour: Plane moves even at the slightest device movement
        //val aHit:HitResult = aResult.last()

        for (aHit in aResult) {
            val aTrackable = aHit.trackable
            if (aTrackable is Plane && (aTrackable.isPoseInPolygon(aHit.hitPose))) {
                anchorA = aHit.createAnchor()
                anchorNodeA = AnchorNode(anchorA)
                anchorNodeA!!.setParent(arSceneView.scene)
                measureElevation()
                /*
                // Debugging
                hTestDistance = aHit.distance
                text_ViewDebug.text = String.format("HRD %.2f",hTestDistance)
                text_ViewDebug.setBackgroundColor(parseColor("#66141414"))*/
                break
            }
            else
            {
                // If user tries to place renderable at non-plane position
                imageViewAR.visibility = View.VISIBLE
                imageViewAR.setImageResource(R.drawable.ic_elevation_renderable)
                imageViewAR.setBackgroundColor(parseColor("#66141414"))
            }
        }
    }


    // Create anchor, place renderable and measure elevation
    private fun measureElevation()
    {
        val vector3 = Vector3(0.00f, 0.1f, 0.00f)
        // Extend created renderable
        mRenderable.thenAccept { material ->
            hRenderable = ShapeFactory.makeCylinder(0.0050f,vector3.y,vector3.scaled(0.5f),material) // 0.1f = 25cm
            hRenderable?.isShadowCaster = false
            hRenderable?.isShadowReceiver = false
        }

        val transformableNodeH = TransformableNode(transformationSystem)
        transformableNodeH.setParent(anchorNodeA)
        transformableNodeH.scaleController.maxScale = 10f //max : (scale * 10)
        transformableNodeH.scaleController.minScale = 0.1f //min : (scale * 10)
        transformableNodeH.rotationController.isEnabled = false
        transformableNodeH.select()
        transformationSystem.selectionVisualizer.removeSelectionVisual(transformableNodeH)
        childNodeA.setParent(transformableNodeH)
        childNodeA.renderable = sRenderable

        debugText = String.format("TNode Local Scale (%.2f,%.2f,%.2f)",transformableNodeH.localScale.x,transformableNodeH.localScale.y,transformableNodeH.localScale.z)
        writeToLog(fragmentTag,debugText)

        //Renderable placed on child node so that, if anchor is changed , there will not be duplicate Renderables
        destNode.setParent(transformableNodeH)
        destNode.renderable = hRenderable
        elevationRenderablePlaced = true
        destNode.worldRotation = anchorNodeA!!.worldRotation
        confirmMeasurementButton.show()
        removeMeasurementButton.show()
        text_ViewInstructions.text = getString(R.string.ar_hRenderable_instructions)

        // Calculate initial height
        val collisionBox = destNode.renderable?.collisionShape as Box
        val heightRenderableSize = collisionBox.size
        val transformableNodeHScale = transformableNodeH.worldScale
        finalElevation = calculateElevation(heightRenderableSize, transformableNodeHScale).toInt()

        val heightString = resources.getString(R.string.height)
        val text = resources.getString(R.string.measure_result,heightString, finalElevation)
        textViewMeasureResult.text = text
        textViewMeasureResult.setBackgroundColor(parseColor("#66141414"))

        /*
        // Debugging
        transformableNodeH.setOnTouchListener { hitTestResult, _ ->
                hTestDistance = hitTestResult.distance
                text_ViewDebug.text = String.format("HRD: %.2f", hTestDistance)
                text_ViewDebug.setBackgroundColor(parseColor("#66141414"))
                true
        }*/

        // Listener to see if user has scaled the renderable
        transformableNodeH.addTransformChangedListener { _, _ ->
            val box = destNode.renderable?.collisionShape as Box
            val hRenSize = box.size
            val tNodeAScale = transformableNodeH.worldScale
            finalElevation = calculateElevation(hRenSize,tNodeAScale).toInt()

            val heightString = resources.getString(R.string.height)
            val text = resources.getString(R.string.measure_result,heightString, finalElevation)
            textViewMeasureResult.text = text

            transformationSystem.selectionVisualizer.removeSelectionVisual(transformableNodeH)
            debugText = String.format("HRenderable Size (%.2f,%.2f,%.2f) tNodeAScale (%.2f,%.2f,%.2f)",hRenSize.x,hRenSize.y,hRenSize.z,tNodeAScale.x,tNodeAScale.y,tNodeAScale.z)
            writeToLog(fragmentTag,debugText)
            // Resize renderable
            vector3.y = hRenSize.y
            debugText = String.format("Final Height (%d) vector3 (%.4f)",finalElevation,vector3.y)
            writeToLog(fragmentTag,debugText)

            // Calculate radius as a scale of transformable node world scale
            val newHRadius = 0.0050f - (0.0050f * (transformableNodeH.worldScale.y/100.0f) * 5.0f)
            debugText = String.format("newHRadius : %.5f",newHRadius)
            writeToLog(fragmentTag,debugText)
            val newBRadius = newHRadius * 2.5f
            debugText = String.format("newBRadius : %.5f",newBRadius)
            writeToLog(fragmentTag,debugText)
            mRenderable.thenAccept { material ->
                hRenderable = ShapeFactory.makeCylinder(newHRadius, vector3.y, vector3.scaled(0.5f), material)
                hRenderable?.isShadowCaster = false
                hRenderable?.isShadowReceiver = false
            }

            mRenderableSV.thenAccept { material ->
                sRenderable = ShapeFactory.makeCylinder(newBRadius,0.0005f,Vector3.zero(),material)
                sRenderable?.isShadowCaster = false
                sRenderable?.isShadowReceiver = false
            }

            destNode.renderable = hRenderable
            destNode.worldRotation = anchorNodeA!!.worldRotation

            // Remake and Replace the base renderable
            childNodeA.renderable = sRenderable
            childNodeA.setParent(transformableNodeH)

            return@addTransformChangedListener
        }
    }

    // Calculate elevation using size of renderable and scale of transformable node
    private fun calculateElevation(renderableSize:Vector3, transformableNodeScale: Vector3): Float {
        //val distanceX = renderableSize.x * transformableNodeScale.x
        val distanceY = renderableSize.y * transformableNodeScale.y
        //val distanceZ = renderableSize.z * transformableNodeScale.z
        return (distanceY * 100.00F)
    }

    // Calculate distance using world coordinates of anchor A and B
    private fun calculateDistance(point1: Vector3, point2: Vector3): Float {
        val distanceX = point1.x - point2.x
        val distanceY = point1.y - point2.y
        val distanceZ = point1.z - point2.z
        return (sqrt((distanceX * distanceX +
                distanceY * distanceY +
                distanceZ * distanceZ)) * 100.00F)
    }

    // Clear measurements when user clicks clear
    private fun clearMeasurements()
    {
        debugText = String.format("Clearing measurements...")
        writeToLog(fragmentTag, debugText)
        val children = ArrayList(arSceneView.scene.children)
        for (node in children)
        {
            if (node is AnchorNode)
            {
                arSceneView.scene.removeChild(node)
                node.anchor!!.detach()
                node.setParent(null)
            }
        }

        anchorNodeA = null
        anchorA = null
        anchorNodeB = null
        anchorB = null
        selectedMovableNode = 0
        distanceRenderablePlaced = false
        elevationRenderablePlaced = false
    }

    // Clean exit if device does not support ARCore
    override fun handleSessionException(sessionException: UnavailableException?) {
        clearMeasurements()
        text_ViewInstructions.text = null
        textViewMeasureResult.text = null
        textViewMeasureResult.background = null
        loadingMessageSnackbar?.dismiss()
        otherMessageSnackbar?.dismiss()
        Toast.makeText(context,R.string.ar_device_incompatible,Toast.LENGTH_LONG).show()
        activity!!.onBackPressed()
    }

    // Show loading message while waiting for the ar plane to load
    private fun showLoadingMessage(textID: Int)
    {
        if (loadingMessageSnackbar != null && loadingMessageSnackbar!!.isShownOrQueued) {
            return
        }

        loadingMessageSnackbar =
                Snackbar.make(activity!!.findViewById(android.R.id.content), textID,
                        Snackbar.LENGTH_INDEFINITE)
        val textViewSnackbar = loadingMessageSnackbar?.view?.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textViewSnackbar?.maxLines = 5
        loadingMessageSnackbar!!.view.setBackgroundColor(0xbf323232.toInt())
        loadingMessageSnackbar!!.show()
    }

    // Hide loading message and show tap to select after plane is detected
    private fun hideLoadingMessage()
    {
        if (loadingMessageSnackbar == null) {
            return
        }

        loadingMessageSnackbar!!.dismiss()
        loadingMessageSnackbar = null
    }

    private fun writeToLog(tag:String, text:String)
    {
        if(debugPrintsEnable)
        {
            Log.d(tag,text)
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    fun checkIsSupportedDeviceOrFinish(fragment: MeasureFragment): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(fragmentTag, "Sceneform requires Android N or later")
            Toast.makeText(fragment.context, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show()
            //fragment.activity!!.onBackPressed()
            return false
        }
        val openGlVersionString = (activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .deviceConfigurationInfo
                .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < minOpenGLVersion) {
            Log.e(fragmentTag, "Sceneform requires OpenGL ES 3.0 later")
            Toast.makeText(fragment.context, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG).show()
            //fragment.activity!!.onBackPressed()
            return false
        }
        return true
    }
}
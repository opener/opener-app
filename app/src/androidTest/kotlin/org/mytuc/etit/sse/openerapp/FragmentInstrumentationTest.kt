package org.mytuc.etit.sse.openerapp

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4


import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.mytuc.etit.sse.openerapp.info.InfoFragment
import org.mytuc.etit.sse.openerapp.questions.DelfiQuestionFragment

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class FragmentInstrumentationTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()

        assertEquals("org.mytuc.etit.sse.openerapp", appContext.packageName)
    }

    @Test fun testDelfiquestionFragment() {
        // The "fragmentArgs" and "factory" arguments are optional.
        val fragmentArgs = Bundle().apply {
            putInt("selectedListItem", 0)
        }
        //val factory = FragmentFactory()
        val scenario = launchFragmentInContainer<DelfiQuestionFragment>(
                fragmentArgs, R.style.AppTheme)
        Thread.sleep(20000) // show fragment for 20 seconds

        //example view test to trigger functions...
        //onView(withId(R.id.text)).check(matches(withText("Hello World!")))

    }

    @Test fun testInfoFragment() {
        // The "fragmentArgs" and "factory" arguments are optional.
        val fragmentArgs = Bundle().apply {
            putInt("selectedListItem", 0)
        }
        //val factory = FragmentFactory()
        val scenario = launchFragmentInContainer<InfoFragment>(
                fragmentArgs, R.style.AppTheme)
        Thread.sleep(20000) // show fragment for 20 seconds

        //example view test to trigger functions...
        //onView(withId(R.id.text)).check(matches(withText("Hello World!")))

    }

}

package org.mytuc.etit.sse.openerapp.info

import org.junit.Test

import org.junit.Assert.*

/**
 * Created on 2019-08-05 by Authors:
 * [Thomas Graichen](mailto:thomas.graichen@etit.tu-chemnitz.de)
 */
class HttpUrlResolverTest {

    @Test
    fun resolveUrisJPG() {
        val imageUris = HttpUrlResolver("https://www-user.tu-chemnitz.de/~grait/opener/pics/").resolveUris("pavement/paved/")
        imageUris.forEach {
            println(it)
        }
    }
    @Test
    fun resolveUrisPNG() {
        val imageUris = HttpUrlResolver("https://www-user.tu-chemnitz.de/~grait/opener/pics/").resolveUris("3216_a/Yes/")
        imageUris.forEach {
            println(it)
        }
    }
}